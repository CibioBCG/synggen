

![](./doc/synggen_logo.png)

Synggen is a tool for fast and scalable generation of platform-specific cancer and matched control whole-exome sequencing (WES) and targeted sequencing (TS) synthetic data. Synggen allows to incorporate phased Single Nucleotide Polymorphisms (SNPs) and complex and allele-specific cancer genomic events (PMs, INDELs, CNAs).


On this page:

  * [News](#markdown-header-news)
  * [Cite](#markdown-header-news)
  * [Install](#markdown-header-installation)
  * [Usage](#markdown-header-usage)
      
      * [Options](#markdown-header-options)
      * [Formats](#markdown-header-synggen-specific-formats)
	  * [Model gen](#markdown-header-reference-model-generation)
      * [Reads gen](#markdown-header-synthetic-reads-generation)
    
  * [BAM / Pileup](#markdown-header-from-fastq-reads-to-pileups)
  * [Try it!](#markdown-header-try-it-yourself)
  * [Licence](#markdown-header-licence)
  * [Contacts](#markdown-header-contacts)

---

News
====================

**version 1.6** 

  * INDELs can now be simulated
  * Fixes on the standard insert size model and support for a new insert size model (`insmode=1`) to exactly resemble reference data insert size distribution

---

Cite
====================

Riccardo Scandino, Federico Calabrese, Alessandro Romanel. **Synggen: fast and data-driven generation of synthetic heterogeneous NGS cancer data**. _Bioinformatics_, 2022. [[doi]](https://doi.org/10.1093/bioinformatics/btac792)

---

Installation
====================

## Compilation from source code

To install synggen on GNU/Linux, clone the repository and compile the C source code.

```bash
git clone https://CibioBCG@bitbucket.org/CibioBCG/synggen.git  
cd synggen
make -f Makefile
```

Binaries are also available in the `bin` folder.

---

Usage
====================

Two modes available

```
Generate Reference Models 
 ./synggen mode=0 bamlist=string bed=string fasta=string commonsnps=string [seqprot=int] [mrq=int] [mbq=int] [minvaf=double] [out=string] [threads=int]

Generate Synthetic Reads
 ./synggen mode=1 bed=string fasta=string rdm=string qm=string [pbe=string] [snps=string] [cnv=string] [pm=string] [indel=string] [seqprot=int] [insmode=int] [insize=int] [insizestd=float] [tc=double] [nreads=int] [rlen=int] [out=string] [threads=int] [seed=int]
```


![](./doc/usage.png)


### Options

| Parameter | Type | Description | Default |
|-------|------|------------ | ------|
| `bamlist` | string | List of BAM files paths | |
| `bed` | string | List of genomic captured regions in BED format | |
| `fasta` | string | Reference genome FASTA | |
| `commonsnps` | string | Common SNPs catalogue (VCF format) | |
| `seqprot` | int | Sequencing protocol (single-end = 0, paired-end = 1) | 0 |
| `mrq` | int | Min read quality for PBE calculation | 10 |
| `mbq` | int | Min base quality for PBE calculation | 10 |
| `minvaf` | double | Minimum VAF to include a genomic position in the PBE model | 0.01 |
| `out` | string | Output folder name | |
| `threads` | int | Number of threads used (if available) for the computation | 1 |
| `rdm` | string | Read depth model (RDM) file path | |
| `pbe` | string | Pair Base Error model (PBE) file path | |
| `qm` | string | Quality model (QM) file path | |
| `snps` | string | Germline SNPs (synggen format) file path | |
| `cna` | string | Somatic copy number aberrations (synngen format) file path | |
| `pm` | string | Somatic point mutations (synngen format) file path | |
| `indel` | string | Somatic indels (synngen format) file path | |
| `insmode` | int | Insert size model creation (0 = MEAN and STD, 1 = DISTRIBUTION) | 0 |
| `insize` | int | Use a specific insert size for data generation | 100 |
| `insizestd` | double | Use a specific insert size STD for data generation | 5 percent of the insert size |
| `tc` | double | Tumor content value for tumor sample generation | 0.0 |
| `nreads` | int | Number of reads to generate | 1000 |
| `rlen` | int | Length of the reads | 100 |
| `seed` | int | Reads generation seed, should be >0 | random |



### Synggen specific formats

For its full capabilities synggen requires two types of custom specific formats.

1) `snps`. Consist of 5 fields, tab separated, no header: 

  * _chromosome_; 
  * _position_; 
  * _alternative_, variant to insert; 
  * _allele1_, if present on allele 1 (0 or 1) ;
  * _allele2_, if present on allele 2 (0 or 1).
  
    E.g.
  
    ```
    chr1    678    A    1    0
    chr1    1400   T    1    1
    chr1    678    G    0    1
    ```
    
    Can be easily obtained from a `vcf` or a pileup file.
    
2) `pm`. Consist of 6 fields, tab separated, no header: 

  * _chromosome_; 
  * _position_; 
  * _alternative_, variant to insert; 
  * _clonality_ of the mutation, from 0 to 1;
  * _whichAllele_, on which allele (allele1 or allele2) does the mutation occur; 
  * _cnAllele_, how many copies of the selected allele have the mutation. 
  
    Note that if a mutation is present on both alleles the entry should be repeated specifying the allele, e.g. 
  
    ```
    chr1    9867    C    1    1    1
    chr1    9867    C    1    2    1
    chr2    5342    G    0.8  1    1
    chr2    7633    A    1    1    3
    ```

3) `indel`. Consist of 7 fields, tab separated, no header: 

  * _chromosome_; 
  * _position_; 
  * _type_, of INDEL, 0 for deletion, 1 for insertion;
  * _sequence_, sequence to delete or to insert; 
  * _clonality_ of the mutation, from 0 to 1;
  * _whichAllele_, on which allele (allele1 or allele2) does the mutation occur; 
  * _cnAllele_, how many copies of the selected allele have the mutation. 
  
    Note that if a mutation is present on both alleles the entry should be repeated specifying the allele, e.g. 
  
    ```
    chr1   939384   1   CT    1.0   1   1
    chr1   939278   0   CGC   0.5   2   1
    ```
    
4) `cna`. Consist of 6 fields, tab separated, no header:

  * _chromosome_; 
  * _start_;
  * _end_; 
  * _cnA_, copy number allele 1;
  * _cnB_, copy number allele 2;
  * _clonality_ of the copy number.
  
    E.g.

    ```
    chr1    678     1375     1    0    1
    chr1    10600   15100    5    1    1
    chr1    32700   54500    0    1    0.3
    ```
  
  

### Reference Model Generation

The files required for the construction of a model are:

- `listOfBams.txt` file containing desired BAMs' paths to use to build the model.
- `bed` format file containing the BAMs captured regions, usually, the used NGS target-kit's .bed file;
- `vcf` format file of common germline SNPs loci. A simple way of generate this file is through intersection between a SNPs catalogue, such as Gencode, and our `bed` with the regions of interest;
- `fasta` of our genome of interest.
- `out` folder path for the generated model files. Model files will have same prefix as the `bed` file used.

Recommended parameters to set are:

- number of `threads` used;
- `seed` value for reproducibility.

E.g. of _listOfBams.txt_ content 

```
path/to/bam1.bam
path/to/bam2.bam
path/to/bam3.bam
```    

**!Note!** Input in the format of `listOfBams.txt` is required even when using a single BAM file. 

Synggen with `mode=0` (default) can be launched as follows 


```
REF=path/to/genome/fasta.fa
BED=path/to/bed.bed
VCF=path/to/vcf.vcf
MODEL_LOC=path/to/model/folder/output/


./synggen mode=0 \
bamlist=path/to/listOfBams.txt \
bed=$BED \
commonsnps=$VCF \
fasta=$REF \
out=$MODEL_LOC \
threads=4 \
seed=42

```

This will generate three model files at the indicated out folder:

- Read depth model file `rdm`;
- Pair Base Error model file `pbe`;
- Quality model file `qm`.

**!Note!** When paired-end reference models are created, insert size information is included in the `rdm` file. By default, the insert size model consists in the `mean` and `std` of insert size values collected while building the reference models. We interpret the insert size as the number of base pairs between the two pairs:

![](./doc/insert_size.png)

Setting parameter `insmode=1` a more advanced approach is activated and the insert size model is stored in the `rdm` file as a whole distribution of values collected when building the reference models. Using this approach, the insert size at data generation will exactly resemble the real distribution observed in the reference files.

  
  
### Synthetic Reads Generation

In order to generate synthetic reads the following files are required:

- `out`, destination folder path for the generated reads;
- `bed`, same as for the model generation; 
- `fasta`, same as for the model generation;
- `rdm` read depth model file generated from synggen `mode=0`;
- `qm` quality model file generated from synggen `mode=0`.

**!Note!** `pbe` model file is recommended but not mandatory for running synggen `mode=1`. 


Germline and somatic components can be introduced in the synthetic reads through the optional parameters:

- `snps`, germline SNPs to introduce. Requires synggen specific snps format;
- `pm`, point mutations to introduce. Requires synggen specific pm format;
- `indel`, indels to introduce. Requires synggen specific indel format;
- `cna`, copy number aberration to introduce. Requires a synggen specific cna format;
- `tc`, tumor content of the generated sample as percentage. Goes from 0 (All generated reads are germline) to 1 (All generated reads are somatic). 

Generated reads specifications can be regulated through different optional params such as `nreads`, `seqprot`, `rlen`.


Synggen with `mode=1` can be launched as follows 



```
REF=path/to/genome/fasta.fa
BED=path/to/bed.bed
MODEL_LOC=path/to/model/folder/output/

READS_LOC=path/to/generated/reads/folder/
MODEL_NAME=nameOfModelFile           #same as Bed used in model generation

SNPS=path/to/synggen/snps/file       #optional
PM=path/to/synggen/pm/file           #optional
CNA=path/to/synggen/cna/file         #optional
NREADS=100000                        #optional
TC=0.2                              #optional

./synggen mode=1 \
bed=$BED \
fasta=$REF \
rdm=${MODEL_LOC}${MODEL_NAME}.rdm.gz \
pbe=${MODEL_LOC}${MODEL_NAME}.pbe.gz \
qm=${MODEL_LOC}${MODEL_NAME}.qm.gz \
snps=$SNPS \
cna=$CNA \
pm=$PM \
threads=$THREADS \
nreads=$NREADS \
tc=$TC \
out=$READS_LOC \
seed=42
```

**!Note!** Synggen `mode=1` will generated a number of `.fastq` files equal to the number of `threads` used. 

**!Note!** Insert size of paired-end reads will be generated using the model available in the input `rdm` file. Setting parameters `insize` and/or `insizestd` will overwrite the the model available in the input `rdm` file.



---

From FASTQ reads to Pileups
====================

## (third party tools pipeline)

Additional pipeline to use once the synggen `.fastq` read files are generated in order to obtain `bam` files and/or pileup files.
Tools required:

- [Samtools](https://www.htslib.org/);
- [BWA](https://github.com/lh3/bwa);
- [PacBam](https://bitbucket.org/CibioBCG/pacbam/src/master/).

Shell script



```
READS_LOC=path/to/generated/reads/folder/
THREADS=16
REF=path/to/genome/fasta.fa
BED=path/to/bed.bed
VCF=path/to/vcf.vcf

echo "Alignment!";
for i in $(ls ${LOCATION}/*.R1.fastq); do
echo $i ;
f=$(echo $i | sed "s/R1.fastq/R1.sam/")
bwa mem -v 1 -M -t ${THREADS} -R "@RG\tID:${LOCATION}\tSM:None\tLB:None\tPL:Illumina" $REF $i > $f;
done 

echo ""
echo "Merging SAMs";
samtools merge -f -@ ${THREADS} ${READS_LOC}/sample.sam ${READS_LOC}/*.R1.sam;

echo ""
echo "SAM to BAM";
samtools view -@ ${THREADS} -S -b -h ${READS_LOC}/sample.sam > ${READS_LOC}/sample.bam;

echo ""
echo "Sorting";
samtools sort  -@ ${THREADS} ${READS_LOC}/sample.bam -o ${READS_LOC}/sample.sorted.bam;

echo ""
echo "Indexing";
samtools index -@ ${THREADS} ${READS_LOC}/sample.sorted.bam;

echo ""
echo "Pileup!";
./pacbam mode=0 \
bam=${LOCATION}/sample.sorted.bam \
bed=$BED \
vcf=$VCF \
fasta=$REF \
threads=$THREADS \
out=$READS_LOC \
mdc=10 \
mrq=20;
```

---

Try it Yourself
====================
Practical and easy to follow example is available for download at [synggen_example](https://bcglab.cibio.unitn.it/synggen_example).
The directory (structure shown below) contains an example `.bam` WES sample (`./data/bams/` sub-directory) with all required files (`./data/` directory) to generate a model 
and to produce synthetic germline and tumor samples (outputs will be generated in `./out/` directory). 
The synggen commands are contained in the script `run.sh`.

```
    📂synggen_example
     ┣ 📂data
     ┃ ┣ 📂bams
     ┃ ┃ ┣ 📜HG00138.bam
     ┃ ┃ ┗ 📜HG00138.bam.bai
     ┃ ┣ 📜CNA.cna
     ┃ ┣ 📜GRCh38.fasta
     ┃ ┣ 📜GRCh38.fasta.amb
     ┃ ┣ 📜GRCh38.fasta.ann
     ┃ ┣ 📜GRCh38.fasta.bwt
     ┃ ┣ 📜GRCh38.fasta.fai
     ┃ ┣ 📜GRCh38.fasta.pac
     ┃ ┣ 📜GRCh38.fasta.sa
     ┃ ┣ 📜PM.pm
     ┃ ┣ 📜INDEL.indel
     ┃ ┣ 📜SNP.snps
     ┃ ┣ 📜regions.bed
     ┃ ┗ 📜regions.vcf
     ┣ 📂out
     ┣ 📜README
     ┣ 📜align.sh
     ┣ 📜listOfBams.txt
     ┣ 📜run.sh
     ┗ 📦synggen
```
  
  
Download and follow the **README**!


**!Note!** If synggen executable does not work recompile it as shown [here](#markdown-header-installation), rename it as *synggen* and move it in the example directory (or modify the SYNGGEN variable path contained in `run.sh`). 



---

Licence
====================

Synggen is released under [MIT](https://bitbucket.org/CibioBCG/synggen/src/master/COPYING) licence.


---


Contacts
====================

[Laboratory of Bioinformatics and Computational Genomics](https://bcglab.cibio.unitn.it), Department CIBIO, University of Trento.


---




