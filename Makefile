all: dynamic

LIBRARIES = -lbcf -lbam -lm -lz -lpthread
LIBRARIESDIR = /shares/CIBIO-Storage/BCG/scratch1/synggen/synggen/lib/
INCLUDESDIR = /shares/CIBIO-Storage/BCG/scratch1/synggen/synggen/include/
CC = gcc
CFLAGS = -g -w -Wall -O2
APPNAME = synggen

dSFMT.o: src/dSFMT.c src/dSFMT.h
	$(CC) -I$(INCLUDESDIR) -DDSFMT_MEXP=216091 -c src/dSFMT.c

dynamic: dSFMT.o src/main.c
	$(CC) $(CFLAGS) -I$(INCLUDESDIR) dSFMT.o src/main.c src/copy_number.c src/generate_reads.c src/input_param.c src/load_data.c src/model_pbe.c src/model_qm.c src/model_rdm.c src/pileup.c src/pointMutation.c src/indels.c src/SNP_model.c src/struct.c -o $(APPNAME) -L$(LIBRARIESDIR) $(LIBRARIES)

static: dSFMT.o src/main.c
	$(CC) $(CFLAGS) -I$(INCLUDESDIR) dSFMT.o src/main.c src/copy_number.c src/generate_reads.c src/input_param.c src/load_data.c src/model_pbe.c src/model_qm.c src/model_rdm.c src/pileup.c src/pointMutation.c src/indels.c src/SNP_model.c src/struct.c -o $(APPNAME) -L$(LIBRARIESDIR) $(LIBRARIES) -static

clean:
	rm -f *.o
