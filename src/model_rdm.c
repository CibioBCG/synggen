#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <math.h>
#include <assert.h>

#include "model_rdm.h"

void computeRDMCumulative(struct target_info *target_regions,int indexInit, int indexEnd, float adm)
{
	int i,r;
	int sumN = 0;
	int sumT = 0;
	int sumloc;
	for (r=indexInit;r<indexEnd;r++)
	{
		i=0;
			sumloc=0;
			sumN += target_regions->info[r]->rdata->aN;
			target_regions->info[r]->rdata->aN = sumN;

			while(i<target_regions->info[r]->to-target_regions->info[r]->from)
			{
				sumloc+=target_regions->info[r]->rdata->positions[i].number;
				target_regions->info[r]->rdata->positions[i].number=sumloc;
				i++;
			}
	 }
}

void computeRDM(struct target_info *target_regions,int indexInit, int indexEnd, int offset)
{
	int i,r;
	double frac;

	for(r=indexInit;r<indexEnd;r++)
	{
		i=0;
		while(i<target_regions->info[r]->to-target_regions->info[r]->from-offset)
		{
			target_regions->info[r]->rdata->aN+=target_regions->info[r]->rdata->positions[i].number;
			i++;
		}

		frac = (double)(target_regions->info[r]->rdata->aN/target_regions->info[r]->rdata->arguments->bamfiles_number);
		target_regions->info[r]->rdata->aN = (int)ceil(frac);
		}
}

void computeRDMCleanEnd(struct target_info *target_regions,int indexInit, int indexEnd, int offset)
{
	int i,r;
	for(r=indexInit;r<indexEnd;r++)
	{
		i=(target_regions->info[r]->to-offset)-target_regions->info[r]->from;
		while(i<target_regions->info[r]->to-target_regions->info[r]->from)
		{
			target_regions->info[r]->rdata->positions[i].number = 0;
			i++;
		}
	}
}

void printRDM(gzFile *outfile, struct target_info *target_regions,int indexInit, int indexEnd)
{
	int i,r;
	for (r=indexInit;r<indexEnd;r++)
	{
		gzprintf(outfile,"%d",target_regions->info[r]->rdata->aN);
		i=0;
		while(i<target_regions->info[r]->to-target_regions->info[r]->from)
		{
			gzprintf(outfile,";%d",target_regions->info[r]->rdata->positions[i].number);
			i++;
		}
		gzprintf(outfile,"\n");
	}
}

int loadInsertSizeFromRDM(char *file_name,struct input_args *arguments, int *ins_distr)
{
	gzFile *file = gzopen(file_name,"r");
	char line[MAX_READ_BUFF];
	char *tmp;
	int i;
	int control = 0;
	int columns = 0;
	char sep[] = " ";
	double tmp_ins;

	// Read Insert Size
	tmp = gzgets(file,line,sizeof(line));
	if(tmp != Z_NULL)
	{
		if (line[0]!='#')
		{
			gzclose(file);
	    	return(0);
		} else
		{
			if (control == 0)
			{
				// count the number of columns
				for(i=0;i<strlen(line);i++)
				{
					if (strncmp(&line[i],sep,1)==0)
						columns++;
				}
				columns++;
				control = 1;
			}

			// tokenize a line
			char *str_tokens[columns];
			char *pch;
			pch = strtok(line," ");
			i=0;
			while (pch != NULL)
			{
				str_tokens[i++]=pch;
				pch = strtok(NULL," ");
			}

			if(strncmp(str_tokens[0],"#0",2) == 0)
			{
				arguments->insert_size = (int)atof(str_tokens[1]);
				arguments->std = (int)atof(str_tokens[2]);
				arguments->ins_mode = 0;
			} else
			{
				ins_distr[0] = atoi(str_tokens[1]);
				tmp_ins = 0.0;
				for(i=2;i<columns;i++)
				{
					ins_distr[i-1] = atoi(str_tokens[i])+ins_distr[i-2];
					tmp_ins += (double)(atoi(str_tokens[i])*(i-1));
				}
				arguments->insert_size = (int)(tmp_ins/(double)ins_distr[1499]);
				tmp_ins = 0.0;
				for(i=0;i<1500;i++)
				{
					tmp_ins += (double)(atoi(str_tokens[i+1]))*pow((double)(i-arguments->insert_size),2.0);
				}
				arguments->std = sqrt(tmp_ins/(double)(ins_distr[1499]));
				arguments->insert_size = arguments->insert_size-500;
				arguments->ins_mode = 1;
			}
		}
	} 

	gzclose(file);
	return(1);
}

void loadRDM(char *file_name,struct target_info *targets, struct input_args *arguments)
{
	gzFile *file = gzopen(file_name,"r");
	char *seq,*tmp_seq,s[200],stmp[200];
	int i,len;

	if(file==NULL)
	{
	   fprintf(stderr, "\n%s\xe2\x9c\x98 ERROR%s: RDM model not present\n", RED, RESET_COL);
	   exit(1);
	}

	// get number of lines
	int number_of_lines = 0;

	char line[MAX_READ_BUFF];
	while(gzgets(file,line,sizeof(line)) != Z_NULL )
	{
	   i=0;
	   while (isspace(line[i])) {i++;}
	   if (line[i]!='#')
	   {
	      number_of_lines++;
	   }
	}

	gzrewind(file);
	if(number_of_lines!=targets->length)
	{
	   printf("%d %d\n",number_of_lines,targets->length);
	   fprintf(stderr, "\n%s\xe2\x9c\x98 ERROR%s: BED target regions specification and RDM model are not compatible.\n", RED, RESET_COL);
	   exit(1);
	}

	int index,r,j;
	int control = 0;
	int columns;
	int line_numb = 1;
	char sep[] = ";";
	char *pch;

	r=0;
	while(gzgets(file,line,sizeof(line)) != Z_NULL )
	{
		j=0;
	   	while (isspace(line[j])) {j++;}
		if (line[j]!='#')
		{
			columns=0;
			for(i=0;i<strlen(line);i++)
			{
				if (strncmp(&line[i],sep,1)==0)
					columns++;
			}
			columns++;

			if ((columns-1)!=(targets->info[r]->to-targets->info[r]->from))
			{
				fprintf(stderr, "\n%s\xe2\x9c\x98 ERROR%s: BED target regions specification and RDM model are not compatible.\n", RED, RESET_COL);
				exit(1);
			}

			pch = strtok(line,";");
			targets->info[r]->rdata->aN = atoi(pch);

			index = 0;
			pch = strtok(NULL,";");
			while (pch != NULL)
			{
				targets->info[r]->rdata->positions[index++].number=atoi(pch);
				pch = strtok(NULL,";");
			}
			r++;
		}
	}
	gzclose(file);
}
