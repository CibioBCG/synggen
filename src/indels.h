#ifndef PM_H
#include "struct.h"
#define PM_H

#if defined(__cplusplus)
extern "C" {
#endif

int loadINDELs(char *file_name,struct target_info *target_regions, struct chr_idx **chrMap);

void introduceINDELs(char s[], int init, int end, struct region_data *elem, dsfmt_t *dsfmt, double af, int *quality_read, int **qualities);

#if defined(__cplusplus)
}
#endif

#endif