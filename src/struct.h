#ifndef STRUCT_H
#include "samtools/sam.h"
#include "samtools/faidx.h"

#define DSFMT_MEXP 216091
#include "dSFMT.h"

#define STRUCT_H

#define CHR_MAP_IDX 24

#define MAX_READ_BUFF 1000000

extern const char RED[];
extern const char GREEN[];
extern const char YELLOW[];
extern const char BLUE[];
extern const char CYAN[];
extern const char MAGENTA[];
extern const char RESET_COL[];

#if defined(__cplusplus)
extern "C" {
#endif


struct input_args
{
    char *bed;
    char *bamlist;
    char *probs;
    char *qm;
    char *cnv;
    char **bamfiles;
    char *outdir;
    char *fasta;
    char *snps;
    char *csnps;
    char *pbe;
    char *pm;
    char *indel;
    int bamfiles_number;
    int cores;
    int rlen;
    float std;
    int ins_mode;
    int insert_size;
    double bgprob;
    double adm;
    int mbq;
    int mrq;
    int mode;
    int paired;
    int gen_reads;
    int nreads;
    int seed;
};

struct pos_pileup
{
    int pos;
    int number;
    int commonSNP;
    int A;
    int C;
    int G;
    int T;
    int Tot;
};

struct snp_info
{
    char *chr;
    int pos;
    char *base;
    struct snp_info *next;
};

struct pm_info
{
    char *chr;
    int pos;
    char *base;
    float clonality;
    double n_alleles;
    int allele;
    struct pm_info *next;
};

struct indel_info
{
    char *chr;
    int pos;
    int type;
    char *sequence;
    float clonality;
    double n_alleles;
    int allele;
    struct indel_info *next;
};

struct cnv_info
{
    char *chr;
    int start;
    int end;
    float allele_a1;
    float allele_a2;
    float clonality_cnv;
    int cn_type;
};

struct region_data
{
    int beg;
    int end;
    int end_ext;
    float strand_bias;
    struct pos_pileup *positions;
    char *sequence;
    int aN;
    struct snp_info *snps_a1;
    struct snp_info *snps_a2;
    struct pm_info *pm_a1;
    struct pm_info *pm_a2;
    struct indel_info *indel_a1;
    struct indel_info *indel_a2;
    struct cnv_info *cnv;

    samfile_t *in;
    struct input_args *arguments;
    float af_region;
};

struct target_t
{
    char *chr;
    int from;
    int to;
    struct target_t *next;
    struct region_data *rdata;
};

struct target_info
{
    int length;
    struct target_t **info;
};

struct chr_idx
{
	int elem;
	int prob;
	struct chr_idx *next;
};

struct args_thread
{
    int start;
    int end;
    struct target_info *target_regions;
    char bam[1000];
    struct input_args *arguments;
    int **qualities;
    double ins_n;
    double ins_mean;
    double ins_M2;
    int *ins_distr; 
};

struct args_reads_thread
{
    int reads;
    int index;
    struct target_info *target_regions;
    char name[1000];
    struct input_args *arguments;
    int **qualities;
    int *ins_distr; 
};

struct fetch_reads_t
{
	bam_plbuf_t *buf;
	struct args_thread *args;
};

void printArguments(struct input_args *arguments);

void deleteFile(const char path[]);

void printHelp();

int getChrPos(char *chr);

int getIndexFromBase(char base);

int checkSequence(char *seq);

#if defined(__cplusplus)
}
#endif

#endif
