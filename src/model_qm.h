#ifndef MODEL_QM_H
#include "struct.h"
#define MODEL_QM_H

#ifdef __cplusplus
extern "C" {
#endif

int getBGE(int **qualities,int read_position,dsfmt_t *dsfmt);

int getPBE(int **qualities,int read_position,int min_quality,dsfmt_t *dsfmt);

void fixQM(int **qualities,int rlen);

int smoothQM(int **qualities,int rlen);

int getQMSize(char *file_name);

void loadQM(char *file_name, int rlen, int** qualities);

void computeQM(int **qualities, int rows_number);

void printQM(gzFile *outfile, int rlen, int** qualities);

#ifdef __cplusplus
}
#endif

#endif