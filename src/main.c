#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <sys/resource.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pthread.h>
#include <unistd.h>
#include <fcntl.h>
#include <libgen.h>

#include "input_param.h"
#include "load_data.h"
#include "pileup.h"
#include "generate_reads.h"
#include "SNP_model.h"

#define DSFMT_MEXP 216091
#define VERSION 1
#define SUBVERSION 6
#include "dSFMT.h"

int main(int argc, char *argv[])
{
	struct rusage r_usage;
	time_t start_t, end_t;
	double diff_t;
	time(&start_t);
	int **qualities;
	int snps_loaded;
	int cnv_loaded;
	int i,j,k,h,n;
	char *temp;
	char *tmp_string[100];
	char *outfile_name = NULL;
	char *outfile_name_normal = NULL;
	char *outfile_name_normalR2 = NULL;
	char *outfile_name2 = NULL;
	char stmp[500];
	char cwd[500];
	char choose;
	gzFile *outfile;
	gzFile *outfileR2;
	double isize_mean,isize_M2;
	struct stat sb;
	int *ins_distr;

	fprintf(stderr, "%ssynggen version %d.%d %s \nAlessandro Romanel\nLaboratory of Bioinformatics and Computational Genomics\nCIBIO University of Trento\n\n", YELLOW, VERSION, SUBVERSION, RESET_COL);

	const char *file = argv[1];

    if(argc < 4)
    {
        printHelp();
        return 1;
    }

    printMessage("Load Input Parameters");
    struct input_args *arguments;
    arguments = getInputArgs(argv,argc);

    if(checkInputArgs(arguments)==1)
      return 1;

	#ifdef _WIN32
    int result_code = mkdir(arguments->outdir);
    #else
    mode_t process_mask = umask(0);
    int result_code = mkdir(arguments->outdir, S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IWGRP | S_IXGRP | S_IROTH);
    umask(process_mask);
    #endif

	if(arguments->gen_reads == 0)
	{
		// Allocate qualities matrix
		qualities = malloc(arguments->rlen * sizeof(int*));
		for(int i = 0; i < arguments->rlen; i++) 
			qualities[i] = malloc(41 * sizeof(int));
		for(i=0; i<arguments->rlen; i++)
			for(j=0; j<41; j++)
				qualities[i][j] = 0;
	} else if(arguments->gen_reads == 1)
	{
		if(arguments->qm != NULL)
		{
			arguments->rlen = getQMSize(arguments->qm);
			// Allocate qualities matrix
			qualities = malloc(arguments->rlen * sizeof(int*));	
			for(int i = 0; i < arguments->rlen; i++) 
				qualities[i] = malloc(41 * sizeof(int));
			for(i=0; i<arguments->rlen; i++)
				for(j=0; j<41; j++)
					qualities[i][j] = 0;
		}
	}

	// Initialize INS distr
	ins_distr = (int *)malloc(1500 * sizeof(int));
	for(i=0; i<1500; i++)
	{
		ins_distr[i] = 0;
	}

	struct target_info* target_regions;

	// Initialize Insert Size if paired end
	if(arguments->gen_reads == 1 && arguments->paired == 1)
	{
		if(arguments->insert_size==0)
		{
			i = loadInsertSizeFromRDM(arguments->probs,arguments,ins_distr);
			if(i==0)
			{
				fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: insert size not available in the RDM model.\n",RED, RESET_COL);
				exit(1);
			}
		} else
		{
			arguments->ins_mode = 0;
			fprintf(stderr,"STD:%f\n",arguments->std);
			if(arguments->std==0.0)
			{
				arguments->std = 0.05*((double)arguments->insert_size);
				if(arguments->std<0)
				{
					arguments->std = arguments->std*(-1.0);
				}
			}
		}
	}

	if(arguments->ins_mode == 0)
	{
		free(ins_distr);
	}

	//Divide nreads by 2 if paired end protocols is used
	if(arguments->gen_reads == 1 && arguments->paired==1)
	{
		arguments->nreads = (int)((double)(arguments->nreads)/2.0);
	}

    // Load file BED
	if(arguments->bed != NULL)
	{
		// Create new file name
		strcpy(tmp_string,basename(arguments->bed));
		temp = strrchr(tmp_string,'.');
		*temp = '\0';
		outfile_name = (char*)malloc(strlen(tmp_string)+14);
		sprintf(outfile_name,"%s.extended.bed",tmp_string);

		// Check BED and extend regions while merging when overlapping
		printMessage("Checking and extending target regions");
		checkTargetBed(arguments->bed, arguments, outfile_name);

		// Load extended BED file
		getcwd(cwd,sizeof(cwd));
		chdir(arguments->outdir);
		target_regions = loadTargetBed(outfile_name, arguments);
		chdir(cwd);
		sprintf(stmp, "%s %d%s target regions loaded", CYAN, target_regions->length, RESET_COL);
		fprintf(stderr, " \xe2\x9c\x9b %s\n", stmp);

		// Clean memory and go back to main folder
		free(outfile_name);
	}

	// Create and initialize chromosome map
	struct chr_idx *chrMap[CHR_MAP_IDX];
	InitializeChrMap(target_regions, chrMap);

    // Load copy numbers data
	if(arguments->cnv != NULL && arguments->gen_reads == 1)
	{
		printMessage("Load copy numbers data");
		cnv_loaded = loadCNV(arguments->cnv, target_regions, chrMap, arguments->rlen, arguments);
		sprintf(stmp, "%s %d%s copy number loaded", CYAN, cnv_loaded, RESET_COL);
		fprintf(stderr, " \xe2\x9c\x9b %s\n", stmp);

		// Creation of allelic fraction at captured regions considering the loaded copy number
		create_allelicFraction(target_regions, arguments);
		sprintf(stmp, " create allelic fractions", CYAN, snps_loaded, RESET_COL);
		fprintf(stderr, " \xe2\x9c\x9b %s\n", stmp);
	}

	// Load SNPs
	if(arguments->snps != NULL && arguments->gen_reads == 1)
	{
		printMessage("Load SNPs model");
		snps_loaded = loadSNPModel(arguments->snps, target_regions, chrMap, arguments);
		sprintf(stmp, "%s %d%s snps tagged", CYAN, snps_loaded, RESET_COL);
		fprintf(stderr, " \xe2\x9c\x9b %s\n", stmp);
	}

	// Load common SNPs for reference model creation
  	if(arguments->csnps != NULL && arguments->gen_reads == 0)
	{
		printMessage("Load common SNPs");
		snps_loaded = loadCommonSNPs(arguments->paired, arguments->csnps, target_regions, chrMap);
		sprintf(stmp,"%s %d%s common snps tagged",CYAN, snps_loaded,RESET_COL);
		fprintf(stderr, " \xe2\x9c\x9b %s\n", stmp);
	}

	// Load SNVs (point mutations data)
	if(arguments->pm!=NULL && arguments->gen_reads==1)
	{
		printMessage("Load point mutations data");
		int pms_loaded;
		pms_loaded = loadPM(arguments->pm,target_regions,chrMap);
		sprintf(stmp,"%s %d%s pm loaded",CYAN, pms_loaded, RESET_COL);
		fprintf(stderr, " \xe2\x9c\x9b %s\n", stmp);
	}

	// Load INDELs (indels data)
	if(arguments->indel!=NULL && arguments->gen_reads==1)
	{
		printMessage("Load indels data");
		int indels_loaded;
		indels_loaded = loadINDELs(arguments->indel,target_regions,chrMap);
		sprintf(stmp,"%s %d%s indel loaded",CYAN, indels_loaded, RESET_COL);
		fprintf(stderr, " \xe2\x9c\x9b %s\n", stmp);
	}

	// Generate models
	if(arguments->gen_reads==0)
  	{
		printMessage("Generate Read Depth Model (RDM) and Quality Model (QM)");
		int regions_per_core = ceil(target_regions->length/arguments->cores)+1;
		pthread_t threads[arguments->cores];
		struct args_thread args[arguments->cores];

		for(int j = 0; j < arguments->bamfiles_number; j++)
		{
			sprintf(stmp,"%s Compute counts%s %s (initialized %s%d%s threads)",YELLOW, RESET_COL, arguments->bamfiles[j],CYAN, arguments->cores,RESET_COL);
		 	printf(" \xe2\xa4\xb7 %s\n", stmp);

		 	i=0;
			while (i < arguments->cores)
			{
				// Initialize arguments for each thread
				args[i].start = i*regions_per_core;
				args[i].end = (i+1)*regions_per_core-1;
				args[i].arguments = arguments;
				args[i].target_regions = target_regions;
				args[i].ins_n = args[i].ins_mean = args[i].ins_n = args[i].ins_M2 = 0;
				args[i].qualities = (int **)malloc(arguments->rlen * sizeof(int*));
				args[i].ins_distr = ins_distr;
				sprintf(args[i].bam,"%s",arguments->bamfiles[j]);

				// Allocate and initialize qualities matrix
				for(k = 0; k < arguments->rlen; k++) 
					args[i].qualities[k] = (int *)malloc(41 * sizeof(int));
				for(k=0; k<arguments->rlen; k++)
					for(h=0; h<41; h++)
						args[i].qualities[k][h] = 0;
				
				if(args[i].end >= (target_regions->length-1))
				{
					args[i].end = target_regions->length-1;
				}

				pthread_create(&threads[i],NULL,PileUp,(void*)(&args[i]));
				
				i++;
			}

			// Join threads
		 	for(i=0;i<arguments->cores;i++)
		 	{
			 	pthread_join(threads[i],NULL);
		 	}

			// Compute qualities and insert size stats
			if(arguments->ins_mode == 0)
				isize_mean = isize_M2 = 0;
			for(i=0;i<arguments->cores;i++)
		 	{
				// qualities
			 	for(k=0; k<arguments->rlen; k++)
					for(h=0; h<41; h++)
						qualities[k][h] += args[i].qualities[k][h];
				// insert size
				if(arguments->ins_mode == 0)
				{
					isize_mean += args[i].ins_mean;
					isize_M2 += sqrt(args[i].ins_M2/(args[i].ins_n-1));
				}
			}
			
			if(arguments->ins_mode == 0)
			{
				isize_M2 = isize_M2/arguments->cores;
				isize_mean = isize_mean/arguments->cores;
			}

			for(i=0;i<arguments->cores;i++)
			{
				for(k=0; k<arguments->rlen; k++)
					free(args[i].qualities[k]);
				free(args[i].qualities);
			}
	 	}

		char *temp;
		strcpy(tmp_string,basename(arguments->bed));
		temp = strrchr(tmp_string, '.');
		*temp = '\0';
		outfile_name = (char*)malloc(strlen(tmp_string)+8);

		computeRDMCleanEnd(target_regions,0,target_regions->length,arguments->rlen);
		computeRDM(target_regions,0,target_regions->length,arguments->rlen+1);

		sprintf(outfile_name,"%s.rdm.gz",tmp_string);

		fprintf(stderr," \xe2\x9c\x9b %s Print%s read depth model\n", YELLOW, RESET_COL);

		getcwd(cwd,sizeof(cwd));
		chdir(arguments->outdir);
		outfile = gzopen(outfile_name,"w6h");
		
		if(arguments->paired == 1)
		{
			if(arguments->ins_mode == 0)
			{
				gzprintf(outfile,"#0 %f %f\n",isize_mean,isize_M2);
			} else
			{
				gzprintf(outfile,"#1");
				for(i=0; i<1500; i++)
				{
					gzprintf(outfile," %d",ins_distr[i]);
				}
				gzprintf(outfile,"\n");
			}
		}
		printRDM(outfile,target_regions,0,target_regions->length);
		
		gzclose(outfile);
		free(outfile_name);

		// Print ISIZE and ISIZE SD when paired
		fprintf(stderr, "%s \xe2\x9e\xa4  average insert size:%f SD:%f \xe2\x9c\x94 %s\n",GREEN,isize_mean,isize_M2,RESET_COL);

		// Print quality model
		outfile_name = (char*)malloc(strlen(tmp_string)+7);
		sprintf(outfile_name,"%s.qm.gz",tmp_string);

		outfile = gzopen(outfile_name,"w6h");

		fprintf(stderr," \xe2\x9c\x9b %s Fix and smooth%s quality model\n", YELLOW, RESET_COL);
		fixQM(qualities,arguments->rlen);
		smoothQM(qualities,arguments->rlen);
		fprintf(stderr," \xe2\x9c\x9b %s Print%s quality model\n", YELLOW, RESET_COL);
		printQM(outfile,arguments->rlen,qualities);

		gzclose(outfile);
		free(outfile_name);
		chdir(cwd);

	} else // Load models
	{
		printMessage("Load Read Depth Model");
		if(arguments->probs != NULL)
		{
			fprintf(stderr, "%s \xe2\x9e\xa4  RDM file %s.....loaded \xe2\x9c\x94 %s\n",GREEN, arguments->probs, RESET_COL);

			loadRDM(arguments->probs,target_regions,arguments);
			if(arguments->paired==1)
			{
				sprintf(stmp,"%s %d%s insert size with %s%f%s standard deviation",CYAN,arguments->insert_size,RESET_COL,CYAN,arguments->std,RESET_COL);
				fprintf(stderr, " \xe2\x9c\x9b %s\n", stmp);
			}

			if(arguments->cnv != NULL)
			{
				fprintf(stderr, " \xe2\x9c\x9b %s Copy number%s analysis\n", YELLOW, RESET_COL);
				computeCNV(target_regions,0,target_regions->length,arguments->cnv,cnv_loaded,arguments);
			}

			fprintf(stderr, " \xe2\x9c\x9b %s Compute read depth cumulative distributions%s\n", YELLOW, RESET_COL);
			computeRDMCumulative(target_regions,0,target_regions->length,0.0);

		} else
		{
			fprintf(stderr,"%sWARNING\xe2\x9d\x97%s: RDM file is not defined. \n", MAGENTA, RESET_COL);
			exit(1);
		}

		// Load quality model
		printMessage("Load Quality Model");
		if(arguments->qm != NULL)
		{
			loadQM(arguments->qm,&(arguments->rlen),qualities);
			fprintf(stderr, "%s \xe2\x9e\xa4  QM file %s with read length of %d.....loaded \xe2\x9c\x94 %s\n",GREEN, arguments->qm,arguments->rlen,RESET_COL);

			fprintf(stderr, " \xe2\x9c\x9b %s Compute QM cumulative distributions%s\n", YELLOW, RESET_COL);
			computeQM(qualities,arguments->rlen);

		} else
		{
			fprintf(stderr,"%sWARNING\xe2\x9d\x97%s: QM file is not defined. \n", MAGENTA, RESET_COL);
			exit(1);
		}
	}

	// Generate error model
	if(arguments->gen_reads == 0)
	{
		printMessage("Generate pair base error model (PBEM)");
		computePBE(target_regions, 0, target_regions->length);
		
		char *temp;
		strcpy(tmp_string,basename(arguments->bed));
		temp = strrchr(tmp_string,'.');
		*temp = '\0';

		outfile_name = (char*)malloc(strlen(tmp_string)+8);

		sprintf(outfile_name,"%s.pbe.gz",tmp_string);

		getcwd(cwd,sizeof(cwd));
		chdir(arguments->outdir);
		outfile = gzopen(outfile_name,"w6h");

		fprintf(stderr," \xe2\x9c\x9b %s  Print%s pair base error model\n",YELLOW,RESET_COL);
		printPBE(outfile,target_regions,0,target_regions->length,arguments->bgprob);
	
		gzclose(outfile);
		free(outfile_name);
		chdir(cwd);

	} else
	{
		printMessage("Load pair base error model");
		if(arguments->pbe != NULL)
		{
			loadPBE(arguments->pbe,target_regions);
			fprintf(stderr,"%s \xe2\x9e\xa4  PBE file %s.....loaded \xe2\x9c\x94 %s\n",GREEN,arguments->pbe,RESET_COL);
		} else
		{
			fprintf(stderr,"%sWARNING\xe2\x9d\x97%s: PBE file is not defined. \n",MAGENTA,RESET_COL);
			exit(1);
		}
	}

	getcwd(cwd,sizeof(cwd));
	chdir(arguments->outdir);

	// Generate reads
	if(arguments->gen_reads == 1)
	{
		int reads_per_core = ceil(arguments->nreads/arguments->cores);
		if(arguments->paired==1)
		{
			sprintf(stmp,"Generate %s%d%s paired reads (Initialized %s%d%s threads)", CYAN, arguments->nreads, RESET_COL, CYAN, arguments->cores, RESET_COL);
		} else
		{
			sprintf(stmp,"Generate %s%d%s reads (Initialized %s%d%s threads)", CYAN, arguments->nreads, RESET_COL, CYAN, arguments->cores, RESET_COL);
		}
		printMessage(stmp);
		pthread_t threads_reads[arguments->cores];
		struct args_reads_thread args_reads[arguments->cores];

		fprintf(stderr, " \xe2\x9c\x9b %s Printing fastq files%s\n", YELLOW, RESET_COL);

		j=0;
		while(j<arguments->cores)
		{
			args_reads[j].index = j+1;
			args_reads[j].reads = reads_per_core;
			args_reads[j].target_regions = target_regions;
			args_reads[j].arguments = arguments;
			args_reads[j].qualities = qualities;
			args_reads[j].ins_distr = ins_distr;
			sprintf(args_reads[j].name,"%s",tmp_string);

			pthread_create(&threads_reads[j],NULL,GenerateReads,(void*)(&args_reads[j]));

			j++;
		}

		for(j=0;j<arguments->cores;j++)
		{
			pthread_join(threads_reads[j],NULL);
		}

		fprintf(stderr, "%s \xe2\x9e\xa4  Fastq .....created \xe2\x9c\x94 %s\n",GREEN, RESET_COL);

	}

getrusage(RUSAGE_SELF,&r_usage);
time(&end_t);
diff_t = difftime(end_t, start_t);
printf("\n\n%sComputation end. Execution time:%s %f minutes \n", BLUE, (diff_t/60), RESET_COL);
printf("%sMemory usage:%s %ld kilobytes\n",BLUE, RESET_COL, r_usage.ru_maxrss);

chdir(cwd);
return 0;
}
