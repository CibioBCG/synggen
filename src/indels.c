#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>

#include "indels.h"

int loadINDELs(char *file_name,struct target_info *target_regions, struct chr_idx **chrMap)
{
	FILE *file = fopen(file_name,"r");
	char *seq,*tmp_seq,s[200],stmp[200];
	int i,len,r,pos;
	struct indel_info *indel_tmp;
	struct chr_idx *idx_tmp;

	if  (file==NULL)
	{
		printMessage("Point mutation file not present");
		exit(1);
	}

	int index = 0;
  	int control = 0;
  	int columns = 0;
  	char sep[] = "\t";
  	int line_numb = 1;
  	char *pch;
  	int indel_loaded = 0;

	char line [MAX_READ_BUFF];
	while(fgets(line,sizeof(line),file) != NULL )
	{
		if (control == 0)
		{
			for(i=0;i<strlen(line);i++)
			{
				if(strncmp(&line[i],sep,1)==0)
					columns++;
			}
			columns++;
			control = 1;
		}

		char *str_tokens[columns];

		pch = strtok(line,"\t");
		i=0;
		while (pch != NULL)
		{
			str_tokens[i++]=pch;
			pch = strtok(NULL,"\t");
		}

		if (i<6)
		{
		  fprintf(stderr,"ERROR: at line %d the number of columns is not correct.\n",line_numb);
		  exit(1);
		}

		pos = atoi(str_tokens[1]);
        if(pos<0)
		{
		    fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: at line %d the position is not correct.\n",RED,RESET_COL,line_numb);
		    exit(1);
		}

        int chr_idx = getChrPos(str_tokens[0]);
		if(chr_idx<0)
		{
		    fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: at line %d the chromosome is not correct. Human chromosomes should be specificed.\n",RED,RESET_COL,line_numb);
		    exit(1);
		}
		idx_tmp = chrMap[chr_idx];

        if(atoi(str_tokens[2])!=0 && atoi(str_tokens[2])!=1)
		{
			fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: at line %d the specified type is wrong [0=deletion,1=insertion].\n",RED,RESET_COL,line_numb);
		    exit(1);
		}

		if(checkSequence(str_tokens[3])==0)
        {
            fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: at line %d the specified sequence is not correct (should be made by A, C, G or T characters).\n",RED,RESET_COL,line_numb);
		    exit(1);
        }

		if(strlen(str_tokens[3])>15)
        {
            fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: at line %d the length of the indel is >15 (15 is the maximum length allowed).\n",RED,RESET_COL,line_numb);
		    exit(1);
        }

		if(atof(str_tokens[4])<0.0 || atof(str_tokens[4])>1.0)
		{
			fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: at line %d the specified clonality should be in the range [0,1].\n",RED,RESET_COL,line_numb);
		    exit(1);
		}

		if(atoi(str_tokens[5])<0 || atoi(str_tokens[5])>2)
        {
            fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: at line %d the specified allele is not correct (valid entries are 1 or 2).\n",RED,RESET_COL,line_numb);
		    exit(1);
        }

		if(atoi(str_tokens[5])<0)
        {
            fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: at line %d the specified number of copies should be >0.\n",RED,RESET_COL,line_numb);
		    exit(1);
        }
		
		while(idx_tmp!=NULL)
		{
			r = idx_tmp->elem;
			if (strcmp(str_tokens[0],target_regions->info[r]->chr) == 0 && pos>=target_regions->info[r]->from && pos<=target_regions->info[r]->to)
			{
				indel_tmp = (struct indel_info *)malloc(sizeof(struct indel_info));

				indel_tmp->pos = (pos-target_regions->info[r]->from)-1;

				indel_tmp->chr = (char*)malloc(sizeof(char)*strlen(str_tokens[0])+1);
				strcpy(indel_tmp->chr,str_tokens[0]);
				subSlash(indel_tmp->chr);

                indel_tmp->type = atoi(str_tokens[2]);

				indel_tmp->sequence = (char*)malloc(sizeof(char)*strlen(str_tokens[3])+1);
        		strcpy(indel_tmp->sequence,str_tokens[3]);
	    		subSlash(indel_tmp->sequence);

				// check if deleted sequence is compatible with reference genome
				if (indel_tmp->type==0 && strncmp(indel_tmp->sequence,target_regions->info[r]->rdata->sequence+indel_tmp->pos,strlen(indel_tmp->sequence))!=0)
				{
					fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: at line %d the specified small deletion is not compatible with the reference genome.\n",RED,RESET_COL,line_numb);
					free(indel_tmp->chr);
					free(indel_tmp->sequence);
					free(indel_tmp);
					break;
				} 

				indel_tmp->clonality = atof(str_tokens[4]);
				indel_tmp->allele = atoi(str_tokens[5]);
			
				if(indel_tmp->allele==1)
				{
					indel_tmp->next = target_regions->info[r]->rdata->indel_a1;
					target_regions->info[r]->rdata->indel_a1 = indel_tmp;
				} else
				{
					indel_tmp->next = target_regions->info[r]->rdata->indel_a2;
					target_regions->info[r]->rdata->indel_a2 = indel_tmp;
				}

				indel_tmp->n_alleles = atof(str_tokens[6]);
	    		indel_loaded++;
	    		break;
			}
			idx_tmp = idx_tmp->next;
		}
	}
	return(indel_loaded);
}

void introduceINDELs(char s[], int init, int end, struct region_data *elem, dsfmt_t *dsfmt, double af, int *quality_read, int **qualities)
{
	struct indel_info *indel_tmp;
	double randnumb;
	double allele;
	double clonality;
	double n_alleles;
    int i;

	if(af<=elem->af_region)
	{
		indel_tmp = elem->indel_a1;
		while(indel_tmp!=NULL)
		{
			if(init<=indel_tmp->pos&&end>=indel_tmp->pos)
			{
				if(indel_tmp->allele==1)
				{
					randnumb = dsfmt_genrand_close_open(dsfmt);
					allele = 1.0;
					if(elem->cnv!=NULL)
						allele = elem->cnv->allele_a1;
					n_alleles = indel_tmp->n_alleles;
					if(n_alleles>allele)
						n_alleles = allele;
					clonality = indel_tmp->clonality;
					if(elem->cnv!=NULL)
						if(elem->cnv->cn_type==1)
							clonality=1;
					if(randnumb<=(n_alleles/allele)*clonality)
                    {
                        if(indel_tmp->type==1)
                        {
                            for(i=end;i>indel_tmp->pos+strlen(indel_tmp->sequence);i--)
							{
                                *(s+(i-init)) = *(s+(i-init-strlen(indel_tmp->sequence)));
								quality_read[i-init] = quality_read[i-init-strlen(indel_tmp->sequence)];
							}
                            for(i=indel_tmp->pos;i<indel_tmp->pos+strlen(indel_tmp->sequence);i++)
                                if(i<=end)
								{
                                    *(s+(i-init)) = indel_tmp->sequence[i-indel_tmp->pos];
									quality_read[i-init] = getBGE(qualities,i-init,dsfmt);
								}
                        }
                        if(indel_tmp->type==0)
                        {
                            for(i=indel_tmp->pos+strlen(indel_tmp->sequence);i<=end;i++)
							{
                                *(s+(i-init-strlen(indel_tmp->sequence))) = *(s+(i-init));
								quality_read[i-init-strlen(indel_tmp->sequence)] = quality_read[i-init];
							}
                            for(i=end-strlen(indel_tmp->sequence);i<end;i++)
                                if(i<=elem->end_ext)
								{
                                    *(s+(i-init)) = elem->sequence[i+strlen(indel_tmp->sequence)];
									quality_read[i-init] = getBGE(qualities,i-init,dsfmt);
								}
                        }
                    }
				}
			}
			indel_tmp = indel_tmp->next;
		}
	} else
	{
		indel_tmp = elem->indel_a2;
		while(indel_tmp!=NULL)
		{
			if(init<=indel_tmp->pos&&end>=indel_tmp->pos)
			{
				if(indel_tmp->allele==2)
				{
					
					randnumb = dsfmt_genrand_close_open(dsfmt);
					allele = 1.0;
					if(elem->cnv!=NULL)
						allele = elem->cnv->allele_a2;
					n_alleles = indel_tmp->n_alleles;
					if(n_alleles>allele)
						n_alleles = allele;
					clonality = indel_tmp->clonality;
					if(elem->cnv!=NULL)
						if(elem->cnv->cn_type==1)
							clonality=1;
					if(randnumb<=(n_alleles/allele)*clonality)
                    {
                        if(indel_tmp->type==1)
                        {
                            for(i=end;i>indel_tmp->pos+strlen(indel_tmp->sequence);i--)
							{
                                *(s+(i-init)) = *(s+(i-init-strlen(indel_tmp->sequence)));
								quality_read[i-init] = quality_read[i-init-strlen(indel_tmp->sequence)];
							}
                            for(i=indel_tmp->pos;i<indel_tmp->pos+strlen(indel_tmp->sequence);i++)
                                if(i<=end)
								{
                                    *(s+(i-init)) = indel_tmp->sequence[i-indel_tmp->pos];
									quality_read[i-init] = getBGE(qualities,i-init,dsfmt);
								}
                        }
                        if(indel_tmp->type==0)
                        {
                            for(i=indel_tmp->pos+strlen(indel_tmp->sequence);i<=end;i++)
							{
                                *(s+(i-init-strlen(indel_tmp->sequence))) = *(s+(i-init));
								quality_read[i-init-strlen(indel_tmp->sequence)] = quality_read[i-init];
							}
                            for(i=end-strlen(indel_tmp->sequence);i<end;i++)
                                if(i<=elem->end_ext)
								{
                                    *(s+(i-init)) = elem->sequence[i+strlen(indel_tmp->sequence)];
									quality_read[i-init] = getBGE(qualities,i-init,dsfmt);
								}
                        }
                    }
				}
			}
			indel_tmp = indel_tmp->next;
		}
	}
}