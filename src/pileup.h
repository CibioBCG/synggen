#ifndef PILEUP_H
#include "struct.h"
#define PILEUP_H

#ifdef __cplusplus
extern "C" {
#endif

#define bam_cigar_op(c) ((c)&BAM_CIGAR_MASK)
#define bam_cigar_oplen(c) ((c)>>BAM_CIGAR_SHIFT)

static int fetch_func(const bam1_t *b, void *data);

static int pileup_func(uint32_t tid, uint32_t pos, int n, const bam_pileup1_t *pl, void *data);

void *PileUp(void *args);

#ifdef __cplusplus
}
#endif

#endif
