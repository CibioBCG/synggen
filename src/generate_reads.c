#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <math.h>

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#include "generate_reads.h"

double drand() // uniform distribution, (0..1]
{
	return (rand()+1.0)/(RAND_MAX+1.0);
}

double insert_size() // normal distribution, centered on 0, std dev 1
{
	return sqrt(-2*log(drand())) * cos(2*M_PI*drand());
}

void  *GenerateReads(void *args)
{
	struct args_reads_thread *foo = (struct args_reads_thread *)args;
	uint32_t seed;
	if(foo->arguments->seed<0)
	{
		seed = foo->index*((int)time(NULL));
	} else
	{
		seed = foo->index*foo->arguments->seed;
	}

	dsfmt_t dsfmt;
	double af;
	dsfmt_init_gen_rand(&dsfmt,seed);

	char outfile_name[500];
	outfile_name[0] = '\0';
	sprintf(outfile_name,"%s_%d.R1.fastq",foo->name,foo->index);
	FILE *outfile = fopen(outfile_name,"w");
	FILE *outfile_paired;
	int *qualities_read_1;
	int *qualities_read_2;

    if(foo->arguments->paired==1)
	{
		sprintf(outfile_name,"%s_%d.R2.fastq",foo->name,foo->index);
		outfile_paired = fopen(outfile_name,"w");
	}

	int selected_target,len,j,selected_position,rnd_inSize;
	double randnumb;
	char sequence[foo->arguments->rlen+1];
	char sequence_paired[foo->arguments->rlen+1];

	int ql,tmp;
	char quality[foo->arguments->rlen+1];
	char quality_paired[foo->arguments->rlen+1];
	char quality_current[2];
	quality_current[1] = '\0';
	int kk;

	for(j=0; j < foo->reads; j++)
	{
		sequence[0]='\0';
		selected_target = getBSRandomTargetN(foo->target_regions,&dsfmt);
		selected_position = getBSRandomTargetPosition(foo->target_regions,selected_target,&dsfmt);

		strncpy(sequence,foo->target_regions->info[selected_target]->rdata->sequence+selected_position,foo->arguments->rlen);
		sequence[foo->arguments->rlen] = '\0';

		if(foo->arguments->paired==1)
		{
			kk=0;
			if (foo->arguments->ins_mode==0)
			{
				// this to avoid to generate artifact peaks in the INS distribution
				while((rnd_inSize = (int)ceil(((double)foo->arguments->insert_size) + (double)(foo->arguments->std)*insert_size()))<(-foo->arguments->rlen))
					{ if((++kk)>5) break; }
			} else
			{
				// this to avoid to generate artifact peaks in the INS distribution
				while((rnd_inSize = getBSInsertSizeDistr(foo->ins_distr,&dsfmt))<(-foo->arguments->rlen))
					{ if((++kk)>5) break; }
			}
			//if(rnd_inSize<(-foo->arguments->rlen))
			//	rnd_inSize=(-foo->arguments->rlen);

			if(foo->target_regions->info[selected_target]->from+selected_position+
			   (foo->arguments->rlen*2)+rnd_inSize > foo->target_regions->info[selected_target]->to+
			   (foo->arguments->rlen*2)+(foo->arguments->insert_size+3*(int)(ceil(foo->arguments->std))))
					rnd_inSize = foo->arguments->insert_size;

			strncpy(sequence_paired,foo->target_regions->info[selected_target]->rdata->sequence+selected_position+rnd_inSize+foo->arguments->rlen,foo->arguments->rlen);
			sequence_paired[foo->arguments->rlen] = '\0';
		}

		af = dsfmt_genrand_close_open(&dsfmt);

		//SNPs
		introduceSNPs(sequence, selected_position, selected_position+foo->arguments->rlen-1, foo->target_regions->info[selected_target]->rdata,af);
		if(foo->arguments->paired==1)
			introduceSNPs(sequence_paired,selected_position+rnd_inSize+foo->arguments->rlen,selected_position+rnd_inSize+foo->arguments->rlen*2,foo->target_regions->info[selected_target]->rdata,af);

		qualities_read_1 = introducePBE(sequence,selected_position,selected_position+foo->arguments->rlen,foo->target_regions->info[selected_target],&dsfmt,foo->arguments->rlen,foo->qualities,foo->arguments->mbq,foo->arguments->bgprob);
		if(foo->arguments->paired==1)
			qualities_read_2 = introducePBE(sequence_paired,selected_position+rnd_inSize+foo->arguments->rlen,selected_position+rnd_inSize+foo->arguments->rlen*2,foo->target_regions->info[selected_target],&dsfmt,foo->arguments->rlen,foo->qualities,foo->arguments->mbq,foo->arguments->bgprob);
	
		randnumb = dsfmt_genrand_close_open(&dsfmt);
		if(randnumb>foo->arguments->adm)
    	{
    		if(foo->target_regions->info[selected_target]->rdata->pm_a1!=NULL||foo->target_regions->info[selected_target]->rdata->pm_a2!=NULL)
			{
				introducePMs(sequence,selected_position,selected_position+foo->arguments->rlen-1,foo->target_regions->info[selected_target]->rdata,&dsfmt,af);
				if(foo->arguments->paired==1)
					introducePMs(sequence_paired,selected_position+rnd_inSize+foo->arguments->rlen,selected_position+rnd_inSize+foo->arguments->rlen*2,foo->target_regions->info[selected_target]->rdata,&dsfmt,af);
			}

			if(foo->target_regions->info[selected_target]->rdata->indel_a1!=NULL||foo->target_regions->info[selected_target]->rdata->indel_a2!=NULL)
			{
				introduceINDELs(sequence,selected_position,selected_position+foo->arguments->rlen-1,foo->target_regions->info[selected_target]->rdata,&dsfmt,af,qualities_read_1,foo->qualities);
				if(foo->arguments->paired==1)
					introduceINDELs(sequence_paired,selected_position+rnd_inSize+foo->arguments->rlen,selected_position+rnd_inSize+foo->arguments->rlen*2,foo->target_regions->info[selected_target]->rdata,&dsfmt,af,qualities_read_2,foo->qualities);
			}
		}

		sequence[foo->arguments->rlen] = '\0';
		if(foo->arguments->paired==1)
			sequence_paired[foo->arguments->rlen] = '\0';

		// We assume a sequence is always rlen
		for(ql = 0; ql < foo->arguments->rlen; ql++)
		{
			quality_current[0] = (char)(qualities_read_1[ql]+33);
			strcpy(&quality[ql],&quality_current);
		}
		quality[foo->arguments->rlen] = '\0';

		if(foo->arguments->paired==1)
		{
			for(ql = 0; ql < foo->arguments->rlen; ql++)
			{
				quality_current[0] = (char)(qualities_read_2[ql]+33);
				strcpy(&quality_paired[ql],&quality_current);
			}
			quality_paired[foo->arguments->rlen] = '\0';
		}

		randnumb = dsfmt_genrand_close_open(&dsfmt);
		if(foo->arguments->paired==1)
		{
			reverseSequence(sequence_paired);
			reverseSequence(quality_paired);
			reverseComplement(sequence_paired);
		} else
		{
			if(randnumb>foo->target_regions->info[selected_target]->rdata->strand_bias)
			{
				reverseSequence(sequence);
				reverseSequence(quality);
				reverseComplement(sequence);
			} 
		}

		if(foo->arguments->paired==0)
		{
			fprintf(outfile,"@%d_%d\n%s\n+\n%s\n",j+1,foo->index,sequence,quality);
		}
		if(foo->arguments->paired==1)
		{

			if(randnumb>foo->target_regions->info[selected_target]->rdata->strand_bias)
			{
				fprintf(outfile,"@%d_%d\n%s\n+\n%s\n",j+1,foo->index,sequence_paired,quality_paired);
				fprintf(outfile_paired,"@%d_%d\n%s\n+\n%s\n",j+1,foo->index,sequence,quality);
			} else
			{
				fprintf(outfile,"@%d_%d\n%s\n+\n%s\n",j+1,foo->index,sequence,quality);
				fprintf(outfile_paired,"@%d_%d\n%s\n+\n%s\n",j+1,foo->index,sequence_paired,quality_paired);
			}
		}
		
		free(qualities_read_1);
		if(foo->arguments->paired==1)
			free(qualities_read_2);
	}
  	fclose(outfile);
	if(foo->arguments->paired==1)
		fclose(outfile_paired);
	
}

int getBSInsertSizeDistr(int *ins_distr, dsfmt_t *dsfmt)
{
	int middle,init,end;
	int n = ins_distr[1499];
	int m = 1;
	int res = (int)(dsfmt_genrand_close_open(dsfmt)*((double)(n-m))+((double)m));

	init = 0;
	end = 1499;
	middle = (init+end)/2;

	while(1)
	{
		if(res<ins_distr[0])
		{
			return(-500);
		} else if(end-init==1)
		{
			return(end-500);
		} else if(res>ins_distr[middle])
		{
			init = middle;
			middle = (init+end)/2;
		} else if(res<ins_distr[middle])
		{
			end = middle;
			middle = (init+end)/2;
		} else if(res==ins_distr[middle])
		{
			while(middle-1>=0&&ins_distr[middle]==ins_distr[middle-1])
				middle--;
			return(middle-500);
		} else
		{
			printf("\n%s\xe2\x9c\x98%s Errore\n", RED, RESET_COL);
			exit(0);
		}
	}
}


void reverseSequence(char *s)
{
	if (s == 0)
	{
		return;
	}

	if (*s == 0)
	{
		return;
	}

	char *start = s;
	char *end = start + strlen(s) - 1;
	char temp;

	while (end > start)
	{
		temp = *start;
		*start = *end;
		*end = temp;
		++start;
		--end;
	}
}

void reverseComplement(char s[])
{
    while (*s)
    {
        switch(*s)
        {
        case 'A':
            *s = 'T';
            break;
        case 'G':
            *s = 'C';
            break;
        case 'C':
            *s = 'G';
            break;
        case 'T':
            *s = 'A';
            break;
        }
        ++s;
    }
}

int getBSRandomTargetN(struct target_info *target_regions, dsfmt_t *dsfmt)
{
	int i,init,end,middle,control;
	int n = target_regions->info[(target_regions->length)-1]->rdata->aN;
	int m = 1;
	int res = (int)(dsfmt_genrand_close_open(dsfmt)*((double)(n-m))+((double)m));

	init = 0;
	end = (target_regions->length)-1;
	middle = (init+end)/2;

	while(1)
	{
		if(res<target_regions->info[0]->rdata->aN)
		{
			return(0);
		} else if(end-init==1)
		{
			return(end);
		} else if(res>target_regions->info[middle]->rdata->aN)
		{
			init = middle;
			middle = (init+end)/2;
		} else if(res<target_regions->info[middle]->rdata->aN)
		{
			end = middle;
			middle = (init+end)/2;
		} else if(res==target_regions->info[middle]->rdata->aN)
		{
			while(middle-1>=0&&target_regions->info[middle]->rdata->aN==target_regions->info[middle-1]->rdata->aN)
				middle--;
			return(middle);
		} else
		{
			printf("\n%s\xe2\x9c\x98%s Errore\n", RED, RESET_COL);
			exit(0);
		}
	}
}

int getBSRandomTargetPosition(struct target_info *target_regions,int index, dsfmt_t *dsfmt)
{
	int i,init,end,middle,control;
	int length = target_regions->info[index]->to-target_regions->info[index]->from;
	int n = target_regions->info[index]->rdata->positions[length-1].number;
	int m = 1;
	int res = (int)(dsfmt_genrand_close_open(dsfmt)*((double)(n-m))+((double)m));
	init = 0;
	end = length-1;
	middle = (init+end)/2;

	while(1)
	{
		if(res<target_regions->info[index]->rdata->positions[0].number)
		{
			return(0);
		} else if(end-init==1)
		{
	     return(end);
		} else if(res>target_regions->info[index]->rdata->positions[middle].number)
		{
			init = middle;
			middle = (init+end)/2;
		} else if(res<target_regions->info[index]->rdata->positions[middle].number)
		{
			end = middle;
			middle = (init+end)/2;
		} else if(res==target_regions->info[index]->rdata->positions[middle].number)
		{
			while(middle-1>=0&&target_regions->info[index]->rdata->positions[middle].number==target_regions->info[index]->rdata->positions[middle-1].number)
				middle--;
			return(middle);
		} else
		{
			printf("\n%s\xe2\x9c\x98%s Errore\n", RED, RESET_COL);
			exit(0);
		}
	}
}

int *introducePBE(char s[], int init, int end, struct target_t *elem, dsfmt_t *dsfmt, int rlen, int **qualities, int mbq, int bgprob)
{
	int i,qual,randnumb;
	double prob,randprob,val;

	int *qualities_read = (int *)malloc(rlen*sizeof(int));

	for(i=init;i<end;i++)
	{
		if((i+elem->from)<elem->to)
		{
			if(elem->rdata->positions[i].T>0)
			{
				qualities_read[i-init] = getPBE(qualities,i-init,mbq,dsfmt);
				prob = (double)elem->rdata->positions[i].T/(double)elem->rdata->positions[i].Tot;
			} else
			{
				qualities_read[i-init] = getBGE(qualities,i-init,dsfmt);
				prob = pow(10.0,(-(double)(qualities_read[i-init])/10.0));
			}
		} else
		{
			qualities_read[i-init] = getBGE(qualities,i-init,dsfmt);
			prob = pow(10.0,(-(double)(qualities_read[i-init])/10.0));
		}

		randprob = dsfmt_genrand_close_open(dsfmt);

		if(randprob<prob)
		{
			val = dsfmt_genrand_close_open(dsfmt);
			if ((i+elem->from)<elem->to)
			{
				if(elem->rdata->positions[i].T>0)
				{
					randnumb = (int)(val*((double)elem->rdata->positions[i].T-1)+1);
					if(randnumb<=elem->rdata->positions[i].A)
					{
						*(s+(i-init)) = 'A';
					} else if(randnumb<=elem->rdata->positions[i].C)
					{
						*(s+(i-init)) = 'C';
					} else if(randnumb<=elem->rdata->positions[i].G)
					{
						*(s+(i-init)) = 'G';
					} else
					{
						*(s+(i-init)) = 'T';
					}
				} else
				{
					randprob = val;
					if(randprob<=0.25)
					{
						*(s+(i-init)) = 'A';
					} else if(randprob>0.25&&randprob<0.5)
					{
						*(s+(i-init)) = 'C';
					} else if(randprob>0.5&&randprob<0.75)
					{
						*(s+(i-init)) = 'G';
					} else
					{
						*(s+(i-init)) = 'T';
					}
				}
			} else
			{
				randprob = val;
					if(randprob<=0.25)
					{
						*(s+(i-init)) = 'A';
					} else if(randprob>0.25&&randprob<0.5)
					{
						*(s+(i-init)) = 'C';
					} else if(randprob>0.5&&randprob<0.75)
					{
						*(s+(i-init)) = 'G';
					} else
					{
						*(s+(i-init)) = 'T';
					}
			}
		}
	}

	return(qualities_read);
}
