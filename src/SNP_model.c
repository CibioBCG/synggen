#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <math.h>

#include "SNP_model.h"


int loadSNPModel(char *file_name, struct target_info *target_regions, struct chr_idx **chrMap, struct input_args *arguments)
{
    FILE *file = fopen(file_name,"r");
    char *seq, *tmp_seq, s[200], stmp[200];
    int i,len,r,pos;
    struct snp_info *snp_tmp;
    struct chr_idx *idx_tmp;

    if(file==NULL)
    {
        printMessage("\n%s\xe2\x9c\x98 ERROR%s: SNPs model not present", RED, RESET_COL);
        exit(1);
    }
    else
    {
        fprintf(stderr, "%s \xe2\x9e\xa4  SNP file: %s .....loaded \xe2\x9c\x94 %s\n",GREEN, file_name, RESET_COL);
    }

    int index = 0, control = 0, columns = 0, snps_loaded = 0;
    char sep[] = "\t", *pch;
    int line_numb = 1;
    double randnumb, af;

	char line [MAX_READ_BUFF];

    while(fgets(line,sizeof(line),file) != NULL )
	{
        if (control == 0)
		{
            for(i=0; i<strlen(line); i++)
            {
                if(strncmp(&line[i], sep, 1) == 0)
                {
                    columns++;
                }
            }
            columns++;
            control = 1;
        }

        char *str_tokens[columns];

        pch = strtok(line,"\t");
        i=0;
        while (pch != NULL)
        {
            str_tokens[i++]=pch;
            pch = strtok(NULL,"\t");
        }

        if(i < 4)
        {
            fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: at line %d the number of columns is not correct.\n",RED, RESET_COL, line_numb);
            exit(1);
        }

		pos = atoi(str_tokens[1]);
        if(pos<0)
		{
		    fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: at line %d the position is not correct.\n",RED,RESET_COL,line_numb);
		    exit(1);
		}

        int chr_idx = getChrPos(str_tokens[0]);
		if(chr_idx<0)
		{
		    fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: at line %d the chromosome is not correct. Human chromosomes should be specificed.\n",RED,RESET_COL,line_numb);
		    exit(1);
		}
		idx_tmp = chrMap[chr_idx];

        if(getIndexFromBase(*str_tokens[2])==4)
        {
            fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: at line %d the specified base is not correct (valied entries are A, C, G or T).\n",RED,RESET_COL,line_numb);
		    exit(1);
        }

        if(atoi(str_tokens[3])<0 || atoi(str_tokens[3])>1 || atoi(str_tokens[4])<0 || atoi(str_tokens[4])>1)
        {
            fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: at line %d the specified genotype is not correct (valid entries are 0 or 1 for columns 4 and 5).\n",RED,RESET_COL,line_numb);
		    exit(1);
        }

		while(idx_tmp!=NULL)
		{
            r = idx_tmp->elem;
            if (strcmp(str_tokens[0],target_regions->info[r]->chr) == 0 && pos>=target_regions->info[r]->from && pos<=target_regions->info[r]->to)
            {
                if(atoi(str_tokens[3])==1)
                {
                    snp_tmp = (struct snp_info *)malloc(sizeof(struct snp_info));
                    snp_tmp->pos = (pos-target_regions->info[r]->from)-1;

                    target_regions->info[r]->rdata->positions[snp_tmp->pos].commonSNP = 1;

                    snp_tmp->chr = (char*)malloc(sizeof(char)*strlen(str_tokens[0])+1);
                    strcpy(snp_tmp->chr,str_tokens[0]);
                    subSlash(snp_tmp->chr);

                    snp_tmp->base = (char*)malloc(sizeof(char)*strlen(str_tokens[2])+1);
                    strcpy(snp_tmp->base,str_tokens[2]);
                    subSlash(snp_tmp->base);

                    snp_tmp->next = target_regions->info[r]->rdata->snps_a1;
                    target_regions->info[r]->rdata->snps_a1 = snp_tmp;
                }
                if(atoi(str_tokens[4])==1)
                {
                    snp_tmp = (struct snp_info *)malloc(sizeof(struct snp_info));
                    snp_tmp->pos = (pos-target_regions->info[r]->from)-1;

                    target_regions->info[r]->rdata->positions[snp_tmp->pos].commonSNP = 1;

                    snp_tmp->chr = (char*)malloc(sizeof(char)*strlen(str_tokens[0])+1);
                    strcpy(snp_tmp->chr,str_tokens[0]);
                    subSlash(snp_tmp->chr);

                    snp_tmp->base = (char*)malloc(sizeof(char)*strlen(str_tokens[2])+1);
                    strcpy(snp_tmp->base,str_tokens[2]);
                    subSlash(snp_tmp->base);
                    
                    snp_tmp->next = target_regions->info[r]->rdata->snps_a2;
                    target_regions->info[r]->rdata->snps_a2 = snp_tmp;
                }

                snps_loaded++;
                break;
            }
            idx_tmp = idx_tmp->next;
		 }

    }
	return(snps_loaded);
}

void introduceSNPs(char s[], int init, int end, struct region_data *elem, double af)
{
    struct snp_info *snps_tmp;

    if(af <= elem->af_region)
    {
        snps_tmp = elem->snps_a1;
        while(snps_tmp!=NULL)
        {
            if(init<=snps_tmp->pos&&end>snps_tmp->pos)
            {
                *(s+(snps_tmp->pos-init)) = snps_tmp->base[0];
            }
            snps_tmp = snps_tmp->next;
        }
    }
    else
	{
		snps_tmp = elem->snps_a2;
		while(snps_tmp!=NULL)
		{
			if(init<=snps_tmp->pos&&end>=snps_tmp->pos)
			{
				*(s+(snps_tmp->pos-init)) = snps_tmp->base[0];
            }
			snps_tmp = snps_tmp->next;
		}
	}
}
