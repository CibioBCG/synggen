#ifndef PM_H
#include "struct.h"
#define PM_H

#if defined(__cplusplus)
extern "C" {
#endif

int loadPM(char *file_name,struct target_info *target_regions, struct chr_idx **chrMap);

void introducePMs(char s[], int init, int end, struct region_data *elem, dsfmt_t *dsfmt, double af);

#if defined(__cplusplus)
}
#endif

#endif
