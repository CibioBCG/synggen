#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <ctype.h>

#include "load_data.h"

void printMessage(char *message)
{
  time_t current_time;
  char* c_time_string;

  current_time = time(NULL);
  c_time_string = ctime(&current_time);
  c_time_string[strlen(c_time_string)-1] = '\0';
  fprintf(stderr,"%s[%s]%s # %s\n",MAGENTA, c_time_string, RESET_COL,message);
}

void checkTargetBed(char *file_name, struct input_args *arguments, char *out_file)
{
	char cwd[500];
	FILE *file = fopen(file_name,"r");
	// Change folder
	getcwd(cwd,sizeof(cwd));
	chdir(arguments->outdir);
	FILE *file2 = fopen(out_file,"w");
	chdir(cwd);
	
	int i;

	if(file != NULL)
	{
		fprintf(stderr, "%s \xe2\x9e\xa4  BED file: %s .....loaded \xe2\x9c\x94 %s\n",GREEN, file_name, RESET_COL);
		fprintf(stderr, " \xe2\xa4\xb7 %s Compute:%s extension/merge regions\n",YELLOW, RESET_COL);
	}
	// get number of lines
	int number_of_lines = 0;

	char line[MAX_READ_BUFF];

	while(fgets(line,sizeof(line),file) != NULL )
	{
		int i=0;
		while (isspace(line[i])) {i++;}
		if (line[i]!='#')
		number_of_lines++;
	}

	rewind(file);

	int control = 0;
	int columns = 0;
	char sep[] = "\t";
	int line_numb = 1;
	int count=0;
	int start, end;
	int old_end, old_start;
	char *chr;
	int *save_start, *save_end = 0;
	char old_chr[100];
	char chr_name[100];

	while(fgets(line,sizeof(line),file) != NULL )
	{
		if (control == 0)
		{
			// count the number of columns
			for(i=0;i<strlen(line);i++)
			{
				if (strncmp(&line[i],sep,1)==0)
					columns++;
			}
			columns++;
			control = 1;
		}

		// tokenize a line
		char *str_tokens[columns];
		char *pch;
		pch = strtok(line,"\t");
		i=0;
		while (pch != NULL)
		{
			str_tokens[i++]=pch;
			pch = strtok(NULL,"\t");
		}

		if (i<3)
		{
			fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: at line %d the number of columns is not correct (at least 3 columns required).\n",RED, line_numb, RESET_COL);
			exit(1);
		}

		chr = str_tokens[0];
		if(getChrPos(str_tokens[0])<0)
		{
			fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: at line %d the chromosome is not correct. Human chromosomes should be specificed.\n", RED, RESET_COL,line_numb);
			exit(1);
		}

		start = atoi(str_tokens[1])-arguments->rlen;
		if(arguments->paired==1)
			start = start-arguments->rlen;
		if(start<0) start=0;
		end = atoi(str_tokens[2])+arguments->rlen;
		if(arguments->paired==1)
			end = end+arguments->rlen;

		if(end<=start || start<0 || end<0)
		{
			fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: BED file is malformed at line %d.\n", RED, RESET_COL,line_numb);
			exit(1);
		}

		if(line_numb > 1)
		{
			if(strcmp(old_chr,chr) != 0)
			{
				// last region
				fprintf(file2,"%s",old_chr);
				fprintf(file2,"\t");
				fprintf(file2,"%d",old_start);
				fprintf(file2,"\t");
				fprintf(file2,"%d\n",old_end);
				old_end = old_start = start;
				strcpy(old_chr,chr);
			}

			if(start > old_end)
			{
				fprintf(file2,"%s",old_chr);
				fprintf(file2,"\t");
				fprintf(file2,"%d",old_start);
				fprintf(file2,"\t");
				fprintf(file2,"%d\n",old_end);
				old_start = start;
				old_end = end;
				strcpy(old_chr,chr);
			} else
			{
				old_end = end;
			}
		} else
		{
			old_start = start;
			old_end = end;
			strcpy(old_chr,chr);
		}
		line_numb++;
	}

	fprintf(file2,"%s",old_chr);
	fprintf(file2,"\t");
	fprintf(file2,"%d",old_start);
	fprintf(file2,"\t");
	fprintf(file2,"%d\n",old_end);

	fclose(file);
	fclose(file2);
}

struct target_info* loadTargetBed(char *file_name, struct input_args *arguments)
{
	FILE *file = fopen(file_name,"r");
	char *seq,*tmp_seq,s[100],stmp[100];
	int i,j,len;

	fprintf(stderr, "%s \xe2\x9e\xa4  Bed file: %s .....loaded \xe2\x9c\x94 %s\n",GREEN, file_name, RESET_COL);
	fprintf(stderr, " \xe2\xa4\xb7 %s Load%s final regions\n",YELLOW, RESET_COL);

	// get number of lines
	int number_of_lines = 0;

	char line[MAX_READ_BUFF];
	while(fgets(line,sizeof(line),file) != NULL )
	{
		int i=0;
		while (isspace(line[i])) {i++;}
		if (line[i]!='#')
		number_of_lines++;
	}

	rewind(file);

	// create the overall structure
	struct target_info *target = malloc(sizeof(struct target_info));
	target->info = malloc(sizeof(struct target_t *)*number_of_lines);
	target->length = number_of_lines;

	int index = 0;
	int control = 0;
	int columns = 0;
	char sep[] = "\t";
	char sep_intervals[] = ",";
	int line_numb = 1;

	faidx_t *fasta;
	fasta = fai_load(arguments->fasta);

  	while(fgets(line,sizeof(line),file) != NULL )
	{
		if (control == 0)
		{
			// count the number of columns
			for(i=0;i<strlen(line);i++)
			{
				if (strncmp(&line[i],sep,1)==0)
						columns++;
			}
			columns++;
			control = 1;
		}

		// tokenize a line
		char *str_tokens[columns];
		char *pch;
		pch = strtok(line,"\t");
		i=0;
		while (pch != NULL)
		{
			str_tokens[i++]=pch;
			pch = strtok(NULL,"\t");
		}

	  	if (i<3)
		{
			fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: at line %d the number of columns is not correct (at least 3 columns required).\n",RED, line_numb, RESET_COL);
			exit(1);
		}

		struct target_t *current_elem = malloc(sizeof(struct target_t));
		int size;

		current_elem->chr = (char*)malloc(strlen(str_tokens[0])+1);

		strcpy(current_elem->chr,str_tokens[0]);
		current_elem->from = atoi(str_tokens[1]);
		current_elem->to = atoi(str_tokens[2]);

		if (str_tokens[3][strlen(str_tokens[3])-1]=='\n')
			size = strlen(str_tokens[3])-1;
		else
			size = strlen(str_tokens[3]);

		current_elem->rdata = malloc(sizeof(struct region_data));
		current_elem->rdata->positions = malloc(sizeof(struct pos_pileup)*(current_elem->to-current_elem->from));

		//current_elem->rdata->beg = current_elem->from;
		//current_elem->rdata->end = current_elem->to;
		current_elem->rdata->aN = 0;
		current_elem->rdata->strand_bias = 0.5;
		current_elem->rdata->af_region = 0.5;
		current_elem->rdata->snps_a1 = NULL;
		current_elem->rdata->snps_a2 = NULL;
		current_elem->rdata->pm_a1 = NULL;
		current_elem->rdata->pm_a2 = NULL;
		current_elem->rdata->indel_a1 = NULL;
		current_elem->rdata->indel_a2 = NULL;
		current_elem->rdata->cnv = NULL;

		for(i = 0; i < (current_elem->to-current_elem->from); i++)
		{
			current_elem->rdata->positions[i].pos = i;
			current_elem->rdata->positions[i].commonSNP = 0;
			current_elem->rdata->positions[i].number = 0;
			current_elem->rdata->positions[i].A = current_elem->rdata->positions[i].C = 0;
			current_elem->rdata->positions[i].G = current_elem->rdata->positions[i].T = 0;
		}

		s[0] = '\0';
		sprintf(stmp,"%s",current_elem->chr);strcat(s,stmp);strcat(s,":");

		if(arguments->paired == 1)
		{
			sprintf(stmp,"%d",current_elem->from+1);strcat(s,stmp);strcat(s,"-");
			current_elem->rdata->end_ext = current_elem->to+(arguments->rlen*2)+(arguments->insert_size+3*(int)(ceil(arguments->std)));
			sprintf(stmp,"%d",current_elem->rdata->end_ext);strcat(s,stmp);
			current_elem->rdata->sequence = fai_fetch(fasta,s,&len);
		} else
		{
			sprintf(stmp,"%d",current_elem->from+1);strcat(s,stmp);strcat(s,"-");
			sprintf(stmp,"%d",current_elem->to+arguments->rlen);strcat(s,stmp);
			current_elem->rdata->end_ext = current_elem->to+arguments->rlen;
			current_elem->rdata->sequence = fai_fetch(fasta,s,&len);
		}

		target->info[index] = current_elem;
		index++;
		line_numb++;
	}

	fai_destroy(fasta);
	return(target);
}

int loadCommonSNPs(int *paired, char *file_name,struct target_info *target_regions, struct chr_idx **chrMap)
{
    FILE *file = fopen(file_name,"r");
    char *seq,*tmp_seq,s[200],stmp[200];
    int i,len,r,pos;
    struct snp_info *snp_tmp;
    struct chr_idx *idx_tmp;

    if(file==NULL)
    {
        fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: SNPs model not present.\n",RED,RESET_COL);
        exit(1);
    } else
    {
        fprintf(stderr, "%s \xe2\x9e\xa4  Common SNPs file: %s .....loaded \xe2\x9c\x94 %s\n",GREEN, file_name, RESET_COL);
    }

	int index = 0;
	int control = 0;
	int columns = 0;
	char sep[] = "\t";
	int line_numb = 1;
	int lines_excluded = 0;
	char *pch;
	double randnumb;
	int snps_loaded = 0;

	char line [MAX_READ_BUFF];
	while(fgets(line,sizeof(line),file) != NULL )
	{
		if(line[0]=='#') // not considering comment lines
		{
			line_numb++;
			continue;
		}

		if (control == 0)
		{
		    // count the number of columns
		    for(i=0;i<strlen(line);i++)
		    {
			if (strncmp(&line[i],sep,1)==0)
			    columns++;
		    }
		    columns++;
		    control = 1;
		}

		// tokenize a line
		char *str_tokens[columns];

		pch = strtok(line,"\t");
		i=0;
		while (pch != NULL)
		{
		    str_tokens[i++]=pch;
		    pch = strtok(NULL,"\t");
		}

		if (i<5)
		{
		    fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: at line %d the number of columns is not correct.\n",RED,RESET_COL,line_numb);
		    exit(1);
		}

		pos = atoi(str_tokens[1]);
		if(pos<0)
		{
		    fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: at line %d the position is not correct.\n",RED,RESET_COL,line_numb);
		    exit(1);
		}

		int chr_idx = getChrPos(str_tokens[0]);
		if(chr_idx<0)
		{
		    fprintf(stderr,"\n%s\xe2\x9c\x98 ERROR%s: at line %d the chromosome is not correct. Human chromosomes should be specificed.\n",RED,RESET_COL,line_numb);
		    exit(1);
		}
		idx_tmp = chrMap[chr_idx];

		if(getIndexFromBase(str_tokens[4][0])==4 || strlen(str_tokens[4])!=1)
		{
			lines_excluded++;
			continue;
		}
		if(getIndexFromBase(str_tokens[3][0])==4 || strlen(str_tokens[3])!=1)
		{
			lines_excluded++;
			continue;
		}

		while(idx_tmp!=NULL)
		{
			r = idx_tmp->elem;
			if(strcmp(str_tokens[0],target_regions->info[r]->chr) == 0 && pos>target_regions->info[r]->from && pos<=target_regions->info[r]->to)
			{
                target_regions->info[r]->rdata->positions[pos-target_regions->info[r]->from-1].commonSNP = getIndexFromBase(str_tokens[4][0])+1;

                snps_loaded++;
                break;
			}
			idx_tmp = idx_tmp->next;
		}
		line_numb++;
	}
	if(lines_excluded>0)
	{
		fprintf(stderr,"%s\xe2\x9c\x98 WARNING%s: %d variants were excluded (only biallelic SNPs are included).\n",BLUE,RESET_COL,lines_excluded);
	}
	return(snps_loaded);
}

void InitializeChrMap(struct target_info *target_regions, struct chr_idx **chrMap)
{
	int i,r,idx;
	struct chr_idx *idx_tmp;
	for(i=0;i<CHR_MAP_IDX;i++)
	{
	   chrMap[i] = NULL;
	}
	for(r=0;r<target_regions->length;r++)
	{
	  idx = getChrPos(target_regions->info[r]->chr);
	  if(idx>=0)
	  {
	     idx_tmp = (struct chr_idx *)malloc(sizeof(struct chr_idx));
	     idx_tmp->elem = r;
	     idx_tmp->next = chrMap[idx];
	     chrMap[idx] = idx_tmp;
	   }
	}
}
