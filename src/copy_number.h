#ifndef CNV_H
#include "struct.h"
#define CNV_H

#if defined(__cplusplus)
extern "C" {
#endif

int loadCNV(char *file_name,struct target_info *target_regions, struct chr_idx **chrMap, struct input_args *arguments);

void computeCNV(struct target_info *target_regions, int indexInit, int indexEnd, char *file_cnv, int length_cnv, struct input_args *arguments);

void create_allelicFraction(struct target_info *target_regions, struct input_args *arguments);

#if defined(__cplusplus)
}
#endif

#endif
